package entities;

import org.json.simple.JSONObject;

import java.util.UUID;

public class Order implements JsonSerializable {
    private String id;
    private double price;
    private long date;


    public Order(){
        id = UUID.randomUUID().toString();
        date = System.currentTimeMillis();
    }

    public String getId() {
        return id;
    }

    public double getPrice() {
        return price;
    }

    public long getDate() {
        return date;
    }

    public JSONObject toJson(){
        JSONObject out = new JSONObject();
        out.put("id", id);
        out.put("price", price);
        out.put("date", date);
        return out;
    }

    void setPrice(double price){
        this.price = price;
    }

    public static class Builder{
        private double price;

        public Builder setPrice(double price){
            this.price = price;
            return this;
        }

        public Order build(){
            Order order = new Order();
            order.setPrice(price);
            return order;
        }
    }
}
