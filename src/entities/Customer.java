package entities;

import org.json.simple.JSONObject;

import java.util.UUID;

public class Customer implements JsonSerializable{
    private String id;
    private String name;
    private String email;

    public Customer(){
        id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public JSONObject toJson(){
        JSONObject json = new JSONObject();
        json.put("id", id);
        json.put("name", name);
        json.put("email", email);
        return json;
    }

    void setName(String name){
        this.name = name;
    }

    void setEmail(String email){
        this.email = email;
    }

    public static class Builder{
        private String name;
        private String email;

        public Builder setName(String name){
            this.name = name;
            return this;
        }

        public Builder setEmail(String email){
            this.email = email;
            return this;
        }

        public Customer build(){
            Customer customer = new Customer();
            customer.setEmail(email);
            customer.setName(name);
            return customer;
        }

    }

}
