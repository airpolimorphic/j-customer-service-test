package entities;

import org.json.simple.JSONObject;

public interface JsonSerializable {
    JSONObject toJson();
}
