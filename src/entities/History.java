package entities;

import exceptions.UserNotFoundException;
import utils.ICustomerWithOrders;
import utils.IListCustomers;
import utils.OrderAddable;
import utils.UserAddable;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class History implements IListCustomers, ICustomerWithOrders, OrderAddable, UserAddable {
    private Map<String, List<Order>> usersOrders = new ConcurrentHashMap<>();
    private Map<String, Customer> customers = new ConcurrentHashMap<>();

    public Collection<Customer> getCustomerList(){
        Collection<Customer> out = customers.values();
        return out;
    }


    public boolean isValidUser(String customerId){
        boolean out = customers.containsKey(customerId) && usersOrders.containsKey(customerId);
        return out;
    }

    public Customer getCustomer(String customerId){
        return customers.get(customerId);
    }

    public List<Order> getCustomerOrders(String customerId){
        return usersOrders.get(customerId);
    }


    public boolean addUser(String name, String email){
        Customer customer = new Customer.Builder()
                .setEmail(email)
                .setName(name)
                .build();

        addCustomer(customer);
        assignCustomerOrders(customer.getId(), new LinkedList<>());
        // linked list because wee didn't need to access random element
        return (isValidUser(customer.getId()));
    }

    public boolean addOrder(String userId, double value){
        boolean isSuccess = false;
        if(!isValidUser(userId)) throw new UserNotFoundException(userId);
        Order order = new Order.Builder()
                .setPrice(value)
                .build();
        isSuccess = usersOrders.get(userId).add(order);
        return isSuccess;
    }


    void addCustomer(Customer customer){
        String customerId = customer.getId();
        customers.put(customerId, customer);
    }

    void assignCustomerOrders(String customerId, List<Order> order){
        usersOrders.put(customerId, order);
    }

}
