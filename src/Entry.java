import entities.History;
import org.json.simple.JSONObject;
import utils.HistoryProxy;

import static spark.Spark.*;

public class Entry {

    static HistoryProxy history; // here will be ConcurrentException in case when you have many users online;
    // so you can't use this code in production
    public static void main(String... args){
        configureServer(args);
        configureHistory();

        get("/list/customers", (req, resp)-> {
            //this will strong reduce speed for access api when you have many users,
            // but is hotfix of concurrentException
            synchronized (history) {
                return history.getListCustomers();
            }
        });

        get("/list/orders/:userid", (req, resp)->{
            String userId = req.params("userid");
            synchronized (history) {
                if (!history.isValidUser(userId)) {
                    JSONObject out = new JSONObject();
                    out.put("error", "no same user with id " + userId);
                    return out.toJSONString();
                }
                return history.getCustomerWithOrders(userId);
            }

        });

        post("/add/user", (req, resp)->{
           String name = req.queryParams("name");
           String email = req.queryParams("email");
           synchronized (history) {
               return history.addNewUser(name, email);
           }
        });


        post("/add/order", (req, resp) ->{

            String userId = req.queryParams("id");
            synchronized (history) {
                if (!history.isValidUser(userId)) {
                    JSONObject out = new JSONObject();
                    out.put("error", "no same user with id " + userId);
                    return out.toJSONString();
                }

                String valueString = req.queryParams("value");
                double value = 0;

                try {
                    value = Double.parseDouble(valueString);
                } catch (RuntimeException parseException) {
                    //must replace to a concrete exception,
                    //i don't remember exactly what is concrete exception - its something from runtime
                    JSONObject out = new JSONObject();
                    out.put("error", valueString);
                    return out.toJSONString();
                }

                return history.addNewOrder(userId, value);
            }
        });
    }

    private static void configureServer(String... args){
        //parameter need if you want read start keys for configure server
        port(8765);
    }

    private static void configureHistory(){
        History historyCore = new History();
        history = new HistoryProxy.Builder()
                .setHistoryCustomers(historyCore)
                .setHistoryCustomersWithOrders(historyCore)
                .setHistoryOrder(historyCore)
                .setHistoryUser(historyCore)
                .build();
    }
}
