package utils;

import entities.Customer;

import java.util.Collection;

public interface IListCustomers {
    Collection<Customer> getCustomerList();
    boolean isValidUser(String id);
}
