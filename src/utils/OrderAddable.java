package utils;

public interface OrderAddable {
    boolean addOrder(String userId, double value);
    boolean isValidUser(String id);
}
