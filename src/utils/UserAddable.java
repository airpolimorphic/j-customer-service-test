package utils;

public interface UserAddable {
    boolean addUser(String name, String email);
    boolean isValidUser(String id);
}
