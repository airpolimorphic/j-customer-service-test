package utils;

import entities.Customer;
import entities.JsonSerializable;
import entities.Order;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.Collection;
import java.util.List;

public class JsonConverter {

    //region SINGLETON
    private JsonConverter(){

    }
    private static JsonConverter converter = new JsonConverter();

    public static JsonConverter instance(){
        return converter;
    }
    //endregion


    public String getJSONListOrders(Customer customer, List<? extends JsonSerializable> orders){
        JSONObject out = new JSONObject();
        out.put("customer", customer.toJson());
        out.put("orders", toJsonArray(orders));
        return out.toJSONString();
    }

    public String getJSONListCustomers(Collection<? extends JsonSerializable> customers){
        JSONObject out = new JSONObject();
        out.put("data", toJsonArray(customers));
        return out.toJSONString();
    }

    public String getJSONIsSuccessOperation(boolean isSuccess){
        JSONObject out = new JSONObject();
        out.put("operation-success", isSuccess);
        return out.toJSONString();
    }


    private JSONArray toJsonArray(Collection<? extends JsonSerializable> collection){
        JSONArray out = new JSONArray();
        for(JsonSerializable entity:collection){
            out.add(entity.toJson());
        }
        return out;
    }
    private JSONArray toJsonArray(List<? extends JsonSerializable> collection){
        JSONArray out = new JSONArray();
        for(JsonSerializable entity:collection){
            out.add(entity.toJson());
        }
        return out;
    }

}
