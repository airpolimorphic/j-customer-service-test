package utils;

import entities.Customer;
import entities.Order;

import java.util.Collection;
import java.util.List;

public class HistoryProxy {

    private ICustomerWithOrders historyCustomersWithOrders;
    private IListCustomers historyCustomers;
    private OrderAddable historyOrder;
    private UserAddable historyUser;

    private HistoryProxy(ICustomerWithOrders historyCustomersWithOrders,
                         IListCustomers historyCustomers,
                         OrderAddable historyOrder,
                         UserAddable historyUser){
        this.historyCustomersWithOrders = historyCustomersWithOrders;
        this.historyCustomers = historyCustomers;
        this.historyOrder = historyOrder;
        this.historyUser = historyUser;
    }

    public String getCustomerWithOrders(String id){
        String out = "";
        Customer customer = historyCustomersWithOrders.getCustomer(id);
        List<Order> orders = historyCustomersWithOrders.getCustomerOrders(id);
        out = JsonConverter.instance().getJSONListOrders(customer, orders);
        return out;
    }

    public String getListCustomers(){
        String out = "";
        Collection<Customer> customers = historyCustomers.getCustomerList();
        out = JsonConverter.instance().getJSONListCustomers(customers);
        return out;
    }

    public String addNewUser(String name, String email){
        String out = "";
        boolean isSuccess = historyUser.addUser(name, email);
        out = JsonConverter.instance().getJSONIsSuccessOperation(isSuccess);
        return out;
    }

    public String addNewOrder(String userId, double value){
        String out = "";
        out = JsonConverter.instance().getJSONIsSuccessOperation(historyOrder.addOrder(userId, value));
        return out;
    }

    public boolean isValidUser(String id){
        return historyOrder.isValidUser(id) &&
                historyUser.isValidUser(id) &&
                historyCustomersWithOrders.isValidUser(id) &&
                historyCustomers.isValidUser(id);
    }



    public static class Builder{
        private ICustomerWithOrders historyCustomersWithOrders;
        private IListCustomers historyCustomers;
        private OrderAddable historyOrder;
        private UserAddable historyUser;

        public Builder setHistoryCustomersWithOrders(ICustomerWithOrders historyCustomersWithOrders) {
            this.historyCustomersWithOrders = historyCustomersWithOrders;
            return this;
        }

        public Builder setHistoryCustomers(IListCustomers historyCustomers) {
            this.historyCustomers = historyCustomers;
            return this;
        }

        public Builder setHistoryOrder(OrderAddable historyOrder) {
            this.historyOrder = historyOrder;
            return this;
        }

        public Builder setHistoryUser(UserAddable historyUser) {
            this.historyUser = historyUser;
            return this;
        }

        public HistoryProxy build(){
            return new HistoryProxy(historyCustomersWithOrders,
                    historyCustomers,
                    historyOrder,
                    historyUser);
        }
    }
}
