package utils;

import entities.Customer;
import entities.Order;

import java.util.List;

public interface ICustomerWithOrders {
    Customer getCustomer(String customerId);
    List<Order> getCustomerOrders(String customerId);
    boolean isValidUser(String id);
}
